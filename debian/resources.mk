resources = \
	resources/css/bootstrap.min.css \
	resources/js/d3.v3.min.js \
	resources/css/fa.min.css \
	resources/js/hogan.min.js

resource_packages = \
	libjs-bootstrap \
	libjs-d3 \
	fonts-font-awesome \
	python3-xstatic-hogan

resources: $(resources)

.PHONY: resources

resources/css/bootstrap.min.css: /usr/share/javascript/bootstrap/css/bootstrap.min.css
	ln -sfT $^ $@

resources/js/d3.v3.min.js: /usr/share/javascript/d3/d3.min.js
	ln -sfT $^ $@

# FIXME: should use a proper libjs-hogan package
resources/js/hogan.min.js: /usr/lib/python3/dist-packages/xstatic/pkg/hogan/data/hogan.js
	ln -sfT $^ $@

resources_tmp += resources/css/fa.css
resources/css/fa.css:
	debian/build-fa > $@ || ($(RM) $@; false)

resources/css/fa.min.css: resources/css/fa.css
	cssmin > $@ < $^ ||  ($(RM) $@; false)
